package com.besedo.jtt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JttApplication {

    public static void main(String[] args) {
        SpringApplication.run(JttApplication.class, args);
    }

}
