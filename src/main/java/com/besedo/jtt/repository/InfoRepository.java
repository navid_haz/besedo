package com.besedo.jtt.repository;


import com.besedo.jtt.dto.Info;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InfoRepository extends JpaRepository<Info, Long> {


    Info findInfoById(long id);
    Page<Info> findAllByBody(String body, Pageable pageable);
    Page<Info> findAllByTitle(String title, Pageable pageable);
    Page<Info> findAllByEmail(String email, Pageable pageable);

}
