package com.besedo.jtt.service;


import com.besedo.jtt.dto.Filter;
import com.besedo.jtt.dto.Info;
import com.besedo.jtt.dto.QueryFilter;
import com.besedo.jtt.repository.InfoRepository;
import com.besedo.jtt.util.StringUtils;
import lombok.RequiredArgsConstructor;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@Service
@RequiredArgsConstructor
public class InfoService {

    private final InfoRepository infoRepository;

    private final static Logger log = Logger.getLogger(InfoService.class.getName());

    public boolean insert(Info info) {
        infoRepository.save(info);
        return true;
    }

    public Page<Info> getInfos(QueryFilter queryFilter, Pageable pageable) {
        List<Info> result = new ArrayList<>();
        if (queryFilter.getFilter().equals(Filter.id)) {
            result.add(infoRepository.findInfoById(Long.parseLong(queryFilter.getValue())));
            return new PageImpl<>(result, pageable, result.size());
        }
        else if (queryFilter.getFilter().equals(Filter.body)){
            return infoRepository.findAllByBody(queryFilter.getValue(), pageable);
        }else if (queryFilter.getFilter().equals(Filter.email)){
            return infoRepository.findAllByEmail(queryFilter.getValue(), pageable);
        }else if (queryFilter.getFilter().equals(Filter.title)){
            return infoRepository.findAllByTitle(queryFilter.getValue(), pageable);
        }else {
            return infoRepository.findAll(pageable);
        }

    }

    public void writeInfosToCsv(PrintWriter writer) {
        List<Info> infos = infoRepository.findAll();
        try (CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT)) {
            csvPrinter.printRecord("ID", "Vowels of body");
            for (Info info : infos) {
                csvPrinter.printRecord(info.getId(), StringUtils.countVowels(info.getBody()));
            }
        } catch (IOException e) {
            log.warning("Error While writing CSV \\n " + e);
        }
    }

}
