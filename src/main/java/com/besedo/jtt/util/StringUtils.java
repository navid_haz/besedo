package com.besedo.jtt.util;

import java.util.Arrays;
import java.util.List;

public class StringUtils {

    public static final List<Character> vowels = Arrays.asList('a', 'e', 'i', 'o', 'u');

    public static int countVowels(String str){
        int result = 0;
        try {
            for (int i = 0; i <str.length() ; i++) {
                char ch = str.charAt(i);
                if (vowels.contains(ch)){
                    result++;
                }
            }
            return result;
        }catch (Exception e){
            e.getStackTrace();
        }
        return -1;
    }

}
