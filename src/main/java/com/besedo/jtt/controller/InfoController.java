package com.besedo.jtt.controller;


import com.besedo.jtt.dto.Info;
import com.besedo.jtt.dto.QueryFilter;
import com.besedo.jtt.service.InfoService;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@RequestMapping("/info")
@RequiredArgsConstructor
public class InfoController {


    private final InfoService infoService;

    @PostMapping
    public ResponseEntity<?> insertInfo(@Valid @RequestBody Info info){

        //only one of title and body is required.
        if (info.getBody().isBlank() && info.getTitle().isBlank()) {
            return ResponseEntity.badRequest().body("At least sending body or title is required");
        }
        return ResponseEntity.ok(infoService.insert(info));
    }

    @GetMapping("paging")
    public Page<Info> getInfos(@Valid @RequestBody QueryFilter queryFilter,
                               @PageableDefault(size = 3, page = 0) Pageable pageable){
        return infoService.getInfos(queryFilter ,pageable);
    }

    @GetMapping(value = "csv")
    public void getInfoByCSV(HttpServletResponse servletResponse) throws IOException {
        servletResponse.setContentType("text/csv");
        servletResponse.addHeader("Content-Disposition","attachment; filename=\"infos.csv\"");
        infoService.writeInfosToCsv(servletResponse.getWriter());
    }

}
