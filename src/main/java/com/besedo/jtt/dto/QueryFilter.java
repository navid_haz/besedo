package com.besedo.jtt.dto;


import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class QueryFilter {

    private Filter filter;

    @NotBlank
    private String value;



}
