package com.besedo.jtt.dto;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.validation.constraints.*;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table
public class Info {

    @Min(1)
    @Max(100)
    @Id
    private Long id;

    @Email
    @NotBlank
    private String email;

    @Size(max = 300)
    private String title;

    @Size(max = 1000)
    private String body;



}
